import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bar {

    public boolean doIt() {
        Logger logger = LogManager.getLogger(Bar.class.getName());
        logger.entry();
        logger.error("Did it again!");
        return logger.exit(false);
    }
}
